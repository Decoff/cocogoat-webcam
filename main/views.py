from django.http.response import JsonResponse
from django.shortcuts import render, redirect
from datetime import datetime
import subprocess

process_datetime = datetime.now()
process_duration = 0

def index(request):
    response = {}
    return render(request, "home.html", response)

def change_conf(request):
    response = {}

    if request.method == "POST":
        resolution = request.POST.get("resolution")
        time = request.POST.get("time")

        filename = "RESULT-" + str(datetime.now()).replace(' ', '_').replace(':', '-')[:19]
        print("Saved to " + filename)

        subprocess.run(["./scripts/conf.sh", resolution, time, filename])
    return redirect("/")

def ping(request):
    if request.method == "POST":
        success = True
        message = ""

        result = subprocess.run(["./scripts/ping.sh"])
        #result = -1

        if result.returncode == 0:
            message = "Ping success"
        else:
            message = "Ping failed"

        response = {
            "success" : success,
            "message" : message
        }
        return JsonResponse(response)
