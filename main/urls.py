from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name='home'),
    path('conf', change_conf, name='change_conf'),
    path('ping', ping, name='ping')
]