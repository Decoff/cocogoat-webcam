ENVBIN=/home/user/Documents/project/env/bin
ENVPIP=$(ENVBIN)/pip
ENVPY=$(ENVBIN)/python3

all: cleanup setenv deploy_prep run

setenv:
	python3 -m venv env
	$(ENVPIP) install -r requirements.txt

static_collect:
	$(ENVPY) manage.py collectstatic --noinput

db_migrate:
	${ENVPY} manage.py makemigrations
	${ENVPY} manage.py migrate

cleanup:
	find /home/user/Documents/project/cocogoat-webcam -name '*.pyc' -delete

run:
	${ENVPY} manage.py runserver

deploy_prep: static_collect db_migrate
