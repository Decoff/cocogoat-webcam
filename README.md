## Cocogoat-webcam application

This is a simple webcam webdriver app

## Anggota

- Adrian Kaiser (1806205615)
- Giffari Faqih Phrasya Hardani (1806205634)
- Timothy Regana Tarigan (1806205041)

## Dependencies

- python3
  ```
  sudo apt update
  sudo apt install python3 python3-pip python3-venv
  ```
- django
  - Install django melalui requirements (disarankan untuk memakai virtual environment python)
    ```
    python3 -m venv env
    env/bin/pip install -r requirements.txt
    ```
- ffmpeg
  ```
  sudo apt install ffmpeg
  ```

## Prerequisites

1. pastikan bahwa driver sudah terinstall pada modules
   - Jika belum, install driver menggunakan
     ```
     cd ./driver
     make clean
     make
     sudo insmod cocodriver.ko
     ```
2. Jika ingin aplikasi webserver berjalan otomatis ketika boot, lakukan:
   ```
   cd ./service
   sudo cp ./service_runserver.service /etc/systemd/system/
   sudo systemctl start service_runserver.service
   ```
   - Jika ingin enable boot script
     ```
     sudo systemctl enable service_runserver.service
     ```
3. Pastikan webserver sudah berjalan (Jika melakukan systemctl start maka aplikasi sudah berjalan)
   ```
   env/bin/python3 manage.py runserver
   ```

## How to use

Setelah webserver menyala, user bisa connect ke webserver melalui localhost:8000

Pada webserver terdapat dua field yang bisa diisi user, yaitu resolution dan duration. resolution adalah besar resolusi dari video yang akan diambil dan duration adalah durasi waktu perekaman dalam detik. Setelah user menginput value, tekan submit untuk merekam.

Ping digunakan untuk mengecek konektivitas.
