$(document).ready(function(){      
    
    $(".ping-button").click(function (event){
        var dataInput={};
        dataInput['csrfmiddlewaretoken'] = csrfToken;

        $.ajax({
            url: 'ping',
            data: dataInput,
            method: "POST",
            success: function(result){
                if(result.success === true){
                    $("#response-modal").find(".modal-body").text(result.message)
                    $("#response-modal").modal()
                    setTimeout(() => {
                        window.location.replace(mainPage);
                    }, 2000);
                }
            }
        })
    })
})